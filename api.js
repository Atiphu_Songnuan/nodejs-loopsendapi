// service test-service start
const express = require("express");
const cors = require("cors");
const http = require("http");
const fs = require("fs");
const readline = require("readline");
http.globalAgent.maxSockets = 5;

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.post("/api/v1/readfile", (req, res) => {
  const readInterface = readline.createInterface({
    input: fs.createReadStream("medservice.log"),
    // output: process.stdout,
    console: false,
  });

  let querySMSJSON = {
    data: [],
  };
  readInterface
    .on("line", function (line) {
      querySMSJSON.data.push(JSON.parse(line));
    })
    .on("close", function (line) {
      querySMSJSON.data.forEach((element, index, querySMSJSON) => {
        // console.log(element);
        let updatepromres = updatesmsno(element);
        console.log("Update Record: " + (index + 1));
        if (index === querySMSJSON.length - 1) {
          updatepromres
            .then((result) => {
              res.send(JSON.parse(result));
              console.log("Updated Success");
            })
            .catch((err) => {
              console.error(err);
              throw err;
            });
        }
      });
    });
  // querySMSJSON.data.push("Hello")
  // console.log9(querySMSJSON);
});

async function updatesmsno(data) {
  let requestdata = JSON.stringify(data);
  //   console.log(requestdata);
  let options = {
    host: "61.19.201.20",
    port: 19539,
    path: "/medservice/opdhomeappointsmsno",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(requestdata),
    },
    rejectUnauthorized: false,
  };

  let updateProm = await new Promise((resolve, reject) => {
    let request = http.request(options, (httpres) => {
      let result = [];
      let data = "";
      httpres.on("data", function (chunk) {
        data += chunk;
      });
      httpres.on("end", function () {
        result = data;
        resolve(result);
      });
    });
    request.on("error", function (e) {
      result = {
        statusCode: 400,
        data: e.message,
      };
      console.log(result);
    //   reject(result);
    });
    request.write(requestdata);
    request.end();
  });
  return updateProm;
}

const port = process.env.PORT || 5555;
app.listen(port, "0.0.0.0", () =>
  console.log("Listening on port " + port + "...")
);
